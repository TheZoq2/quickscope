// top = main::top_test

#include <cstdint>
#include <format>
#define TOP top_test

#include <verilator_util.hpp>

#include <iostream>

#define TICK \
    ctx->timeInc(1); \
    dut->clk_25mhz = true; \
    dut->eval(); \
    ctx->timeInc(1); \
    dut->clk_25mhz = false; \
    dut->eval();


TEST_CASE(it_works, {
    s.i->rst = "true";
    s.i->config = "Config$(samples_before_trigger: 50, num_samples: 150, armed: true)";
    s.i->trigger_value = "250";
    TICK;
    s.i->rst = "false";

    // Wait for the trigger
    for(int i = 0; i < 200; i++) {
        TICK
    }
    // We now expect a the 50 cycles before triggger before the first sample is transmitted

    for(int i = 0; i < 50; i++) {
        TICK;
        ASSERT_EQ(s.o->debug_transmit, "None()")
    }

    while (*s.o->debug_transmit == "None()") {
        TICK
    }

    for(int i = 202; i < 350; i++) {
        std::cout << std::format("Waiting for {}", i) << std::endl;
        uint8_t upper = i >> 8;
        uint8_t lower = i;

        ASSERT_EQ(s.o->debug_transmit, std::format("Some({}u)", upper));
        do  {
            TICK
        } while(*s.o->debug_transmit == std::format("None()", upper));
        std::cout << "Got upper, looking for lower" << std::endl;
        ASSERT_EQ(s.o->debug_transmit, std::format("Some({}u)", lower));
        do  {
            TICK
        } while(*s.o->debug_transmit == std::format("None()", upper));
    }
    // After that we expect a sample to arrive at some 
    for(int i = 0; i < 200000; i++) {
        TICK
        
    }
    return 0;
})


TEST_CASE(ring_buffer_wraparound_works, {
    s.i->rst = "true";
    s.i->config = "Config$(samples_before_trigger: 50, num_samples: 150, armed: true)";
    s.i->trigger_value = std::format("{}u", 1 << 15);
    TICK;
    s.i->rst = "false";

    // Wait for the trigger
    for(int i = 0; i < 32768; i++) {
        std::cout << std::format("i: {}\n", i);
        TICK
    }

    // We now expect a the 50 cycles before triggger before the first sample is transmitted
    for(int i = 0; i < 50; i++) {
        TICK;
        ASSERT_EQ(s.o->debug_transmit, "None()")
    }

    while (*s.o->debug_transmit == "None()") {
        TICK
    }

    for(int i = (1 << 15) - 48; i < (1 << 15) + 100; i++) {
        std::cout << std::format("Waiting for {:x}", i) << std::endl;
        uint8_t upper = i >> 8;
        uint8_t lower = i;

        ASSERT_EQ(s.o->debug_transmit, std::format("Some({}u)", upper));
        do  {
            TICK
        } while(*s.o->debug_transmit == std::format("None()", upper));
        std::cout << "Got upper, looking for lower" << std::endl;
        ASSERT_EQ(s.o->debug_transmit, std::format("Some({}u)", lower));
        do  {
            TICK
        } while(*s.o->debug_transmit == std::format("None()", upper));
    }

    return 0;
})

MAIN
