// top = chunker::chunker_test

#include <cstdint>
#include <format>
#define TOP chunker_test

#include <verilator_util.hpp>

#include <iostream>

#define TICK \
    dut->eval(); \
    ctx->timeInc(1); \
    dut->clk_i = true; \
    dut->eval(); \
    ctx->timeInc(1); \
    dut->clk_i = false; \


TEST_CASE(it_works, {
    s.i->rst = "true";
    TICK;
    s.i->uart = "UartOut::None()";
    s.i->rst = "false";

    s.i->uart = "UartOut::None()";
    for(int i = 0; i < 10; i++) {
        TICK;
        ASSERT_EQ(s.o, "Chunks(None())");
        TICK;
        ASSERT_EQ(s.o, "Chunks(None())");
        TICK;
        ASSERT_EQ(s.o, "Chunks(None())");
    }
    TICK

    s.i->uart = "UartOut::Ok(1)";
    TICK;
    s.i->uart = "UartOut::None()";
    ASSERT_EQ(s.o, "Chunks(None())");
    TICK;


    s.i->uart = "UartOut::Ok(2)";
    TICK;
    ASSERT_EQ(s.o, "Chunks(None())");

    s.i->uart = "UartOut::None()";
    TICK;


    s.i->uart = "UartOut::Ok(3)";
    TICK;

    ASSERT_EQ(s.o, "Chunks(Some((1, 2, 3)))");
    s.i->uart = "UartOut::None()";
    TICK;

    s.i->uart = "UartOut::Ok(10)";
    TICK;
    ASSERT_EQ(s.o, "Chunks(None())");
    s.i->uart = "UartOut::Ok(15)";
    TICK;
    ASSERT_EQ(s.o, "Chunks(None())");
    s.i->uart = "UartOut::Ok(20)";
    TICK
    ASSERT_EQ(s.o, "Chunks(Some((10, 15, 20)))");

    return 0;
})

MAIN
