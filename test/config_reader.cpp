// top = config::config_reader_test_harness

#include <cstdint>
#include <format>
#define TOP config_reader_test_harness

#include <verilator_util.hpp>

#include <iostream>

#define TICK \
    dut->eval(); \
    ctx->timeInc(1); \
    dut->clk_i = true; \
    dut->eval(); \
    ctx->timeInc(1); \
    dut->clk_i = false; \


TEST_CASE(it_works, {
    s.i->rst = "true";
    TICK;
    s.i->chunks = "Chunks(None())";
    s.i->rst = "false";


    ASSERT_EQ(s.o->armed, "false");

    s.i->chunks = "Chunks(Some((2, 0, 1)))";
    TICK
    s.i->chunks = "Chunks(None())";
    ASSERT_EQ(s.o->armed, "true");


    ASSERT_EQ(s.o->num_samples, "300");
    s.i->chunks = "Chunks(Some((1, 0x70u, 0xffu)))";
    TICK
    s.i->chunks = "Chunks(None())";
    ASSERT_EQ(s.o->num_samples, "0x70ffu");


    ASSERT_EQ(s.o->samples_before_trigger, "50");
    s.i->chunks = "Chunks(Some((0, 0x70u, 0xffu)))";
    TICK
    s.i->chunks = "Chunks(None())";
    ASSERT_EQ(s.o->samples_before_trigger, "0x70ffu");

    return 0;
})

MAIN
