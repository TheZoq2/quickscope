mod config;

use std::fs::File;

use camino::Utf8PathBuf;
use color_eyre::{eyre::Context, Result};
use serialport::SerialPort;
use vcd::{SimulationCommand, Value};

pub trait PortExt {
    fn write_u16(&mut self, addr: u8, val: u16) -> Result<()>;
}
impl PortExt for Box<dyn SerialPort> {
    fn write_u16(&mut self, addr: u8, val: u16) -> Result<()> {
        self.write_all(&[addr, (val >> 8) as u8, val as u8])
            .with_context(|| "Failed to write u16")?;
        Ok(())
    }
}

fn write_vcd_file(values: &[u16]) -> Result<()> {
    let vcd_filename = "out.vcd";
    let vcd_file =
        File::create(&vcd_filename).with_context(|| "Failed to create output vcd file")?;
    let mut writer = vcd::Writer::new(vcd_file);

    writer.timescale(1, vcd::TimescaleUnit::NS)?;
    writer.add_module("top")?;
    let wire = writer.add_wire(16, "data")?;
    writer.upscope()?;
    writer.enddefinitions()?;

    writer.begin(SimulationCommand::Dumpvars)?;
    writer.end()?;

    for (i, value) in values.iter().enumerate() {
        writer.change_vector(
            wire,
            format!("{value:b}")
                .chars()
                .map(|c| if c == '1' { Value::V1 } else { Value::V0 }),
        )?;
        writer.timestamp(i as u64)?;
    }

    Ok(())
}

fn main() -> Result<()> {
    color_eyre::install()?;

    let config_file = Utf8PathBuf::from("quickscope.toml");
    let config = if config_file.exists() {
        config::Config::from_file(&config_file)?
    } else {
        config::Config::default()
    };

    let mut port = serialport::new(
        "/dev/serial/by-id/usb-FER-RADIONA-EMARD_ULX3S_FPGA_85K_v3.0.8_D00252-if00-port0",
        921600,
    )
    .flow_control(serialport::FlowControl::None)
    .stop_bits(serialport::StopBits::Two)
    .open()
    .context("Failed to open serial")?;

    let mut sample_queue: Vec<u8> = vec![];

    let mut values = vec![];

    // Configure the scope
    port.write_u16(0, config.samples_before_trigger())
        .context("Failed two write samples_before_trigger")?;
    port.write_u16(1, config.num_samples())
        .context("Failed to write num_samples")?;
    // Arm the scope
    port.write_u16(2, 1).context("Failed to arm the scope")?;

    loop {
        let mut buf: Vec<u8> = vec![0; 1024];
        let count = match port.read(&mut buf) {
            Ok(count) => count,
            Err(e)
                if matches!(
                    e.kind(),
                    std::io::ErrorKind::WouldBlock | std::io::ErrorKind::TimedOut
                ) =>
            {
                continue
            }
            err => err.context("While reading from serial port")?,
        };

        sample_queue.extend(&buf[0..count]);

        if count != 0 {
            let num_chunks = (sample_queue.len() / 2) * 2;

            let full_chunks = sample_queue.drain(0..num_chunks).collect::<Vec<_>>();

            for i in 0..num_chunks / 2 {
                let value = ((full_chunks[i * 2] as u16) << 8) + (full_chunks[i * 2 + 1] as u16);
                values.push(value);

                println!(
                    "Val: {value:04x} | {:02x} {:02x}",
                    (full_chunks[i * 2] as u16),
                    (full_chunks[i * 2 + 1] as u16)
                );

                if values.len() == config.num_samples() as usize {
                    println!("Wrote vcd");
                    write_vcd_file(&values)?;
                    values.clear()
                }
            }
        }
    }
}
