use std::io::Write;

use camino::Utf8Path;
use serde::Deserialize;

use color_eyre::{
    eyre::{bail, Context},
    Result,
};

#[derive(Deserialize, Default)]
pub struct Config {
    num_samples: Option<u16>,
    samples_before_trigger: Option<u16>,
}

impl Config {
    pub fn from_file(filename: &Utf8Path) -> Result<Self> {
        let content = std::fs::read_to_string(filename)
            .with_context(|| format!("Failed to read config from {filename}"))?;

        let result: Self =
            toml::from_str(&content).with_context(|| format!("Failed to decode {filename}"))?;

        result.validate()?;
        Ok(result)
    }

    fn validate(&self) -> Result<()> {
        if self.num_samples.map(|s| s > (1 << 15)).unwrap_or(false) {
            bail!("Num samples must fit in 15 bits")
        }
        if self.num_samples.map(|s| s > (1 << 15)).unwrap_or(false) {
            bail!("samples_before_trigger must fit in 15 bits")
        }
        Ok(())
    }

    pub fn num_samples(&self) -> u16 {
        self.num_samples.unwrap_or(1 << 15)
    }

    pub fn samples_before_trigger(&self) -> u16 {
        self.samples_before_trigger.unwrap_or(1000)
    }
}
