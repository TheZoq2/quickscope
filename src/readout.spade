use std::ports::new_mut_wire;

use protocols::uart::UartConfig;
use protocols::uart::uart_tx;

use lib::main::SampleReadPort;

enum State {
    Idle,
    Write{sample: int<15>, byte: int<2>},
    // Holding stage to not switch back to write until the UART has had time to acknowledge
    // its command
    WriteWaitUart{sample: int<15>, byte: int<2>, duration: int<3>},
    Done,
}

struct ReadoutOut {
    done: bool,
    debug_readout: Option<int<8>>
}

pipeline(6) readout(
    clk: clock,
    rst: bool,
    perform_readout: bool,
    num_samples: int<15>,
    uart_tx_wire: &mut bool,
    uart_config: UartConfig,
    sample_port: SampleReadPort
) -> ReadoutOut {
        let uart_out_ = stage(+6).uart_out;
        let next_state = match stage(+0).state {
            State::Idle => if perform_readout {State::Write$(
                sample: trunc(num_samples - 1),
                byte: 1,
            )} else {
                State::Idle()
            },
            State::Write$(sample, byte) => {
                if uart_out_.ready {
                    if sample == 0 && byte == 0 {
                        State::Done()
                    }
                    else if byte == 0 {
                        State::WriteWaitUart$(sample: trunc(sample - 1), byte: 1, duration: 0)
                    }
                    else {
                        State::WriteWaitUart$(sample, byte: 0, duration: 0)
                    }
                }
                else {
                    State::Write$(sample, byte)
                }
            },
            State::WriteWaitUart$(sample, byte, duration) => {
                if duration == 6u {
                    State::Write$(sample, byte)
                }
                else {
                    State::WriteWaitUart$(sample, byte, duration: trunc(duration + 1))
                }
            },
            State::Done => State::Idle()
        };

        reg(clk) state reset(rst: State::Idle()) = next_state;
    reg;

        let uart_read_addr = match next_state {
            State::Idle => 0,
            State::Write$(sample, byte: _) => sample,
            State::WriteWaitUart(sample, _, _) => sample,
            State::Done => 0,
        };

        let read_port_out = inst(5) lib::main::read_sample_port(clk, sample_port, uart_read_addr);
    reg*5;
        'uart_tx
        let to_transmit = match state {
            State::Idle => None(),
            State::Write$(sample: _, byte) => Some(trunc(read_port_out >> (byte * 8))),
            State::WriteWaitUart$(sample: _, byte, duration: _) => Some(trunc(read_port_out >> (byte * 8))),
            State::Done => None(),
        };

        let uart_out = inst uart_tx(clk, rst, to_transmit, uart_config);
        set uart_tx_wire = uart_out.tx;

        reg(clk) (last_uart_ready, last_to_transmit) = (uart_out.ready, to_transmit);
        let debug_readout = if last_uart_ready && !uart_out.ready {
            last_to_transmit
        } else {
            None()
        };


        ReadoutOut$(
            done: match state {
                State::Write(_, _) => false,
                State::WriteWaitUart(_, _, _) => false,
                State::Idle => false,
                State::Done => true
            },
            debug_readout
        )
}
